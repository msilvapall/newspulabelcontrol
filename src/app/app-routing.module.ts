import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './main/home/home.component';
import { LoginComponent } from './main/login/login.component';
import { BaseComponent } from './modulos/base/base.component';
import { AjustesCuadraturaComponent } from './modulos/ajustes-cuadratura/ajustes-cuadratura.component';
import { AjustesCuadraturaOtComponent } from './modulos/ajustes-cuadratura-ot/ajustes-cuadratura-ot.component';
import { DashboardComponent } from './modulos/dashboard/dashboard.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'base', component: BaseComponent },
  { path: 'ajustes-cuadratura', component: AjustesCuadraturaComponent },
  { path: 'ajustes-cuadratura/ot', component: AjustesCuadraturaOtComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: '', component: HomeComponent },
  { path: '**', redirectTo: '/', pathMatch: 'full' },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
