import { Component, OnInit } from '@angular/core';

// Filtro
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  // Filtro
  navFilterControl = new FormControl();
  navFilterOptions: string[] = ['Módulo 1', 'Módulo 2', 'Módulo 3'];
  navFilteredOptions: Observable<string[]> | undefined;
  // Ususario
  navMenuUserHide = true;

  constructor() { }

  ngOnInit(): void {
    // Filtro
    this.navFilteredOptions = this.navFilterControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  // Filtro
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.navFilterOptions.filter(navFilterOption => navFilterOption.toLowerCase().includes(filterValue));
  }
}
