import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  mainTitle = 'Unifrutti®';
  mainLogged = true; // Temporal (Login)

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
    // Temporal (Login)
    this.router.events.subscribe(
      (event: any) => {
        if (event instanceof NavigationEnd) {
          if (!this.mainLogged) {
            this.router.navigate(['login']);
          } else if (this.router.url === '/login') {
            this.router.navigate(['home']);
          }
        }
      }
    );
  }
}
