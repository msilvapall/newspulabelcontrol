import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  moduloInfo = ['Exp Unifrutti Traders SPA', 'Temporada 2020', 'Teno'];
  moduloNav = [
    { small: 'Label Control', large: 'Label Control', url: '/' },
    { small: 'Home', large: '', url: '' },
  ];

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  // Temporal (Rutas)
  routerLink(page: string, params: any) {
    if (params)
      this.router.navigate(['/'+page], { queryParams: params });
    else
      this.router.navigate(['/'+page]);
  }

}
