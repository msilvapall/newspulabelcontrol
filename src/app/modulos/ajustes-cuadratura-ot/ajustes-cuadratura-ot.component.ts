import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop'; // Temporal (Comparativa)

@Component({
  selector: 'app-ajustes-cuadratura-ot',
  templateUrl: './ajustes-cuadratura-ot.component.html',
  styleUrls: ['./ajustes-cuadratura-ot.component.css']
})
export class AjustesCuadraturaOtComponent implements OnInit {
  moduloInfo = ['Exp Unifrutti Traders SPA', 'Temporada 2020', 'Teno'];
  moduloNav = [
    { small: 'Label Control', large: 'Label Control', url: '/' },
    { small: 'Ajustes cuadratura', large: 'Listado folios por OT', url: '/ajustes-cuadratura' },
    { small: 'OT', large: 'Ajuste de OT', url: '' },
  ];

  moduloSelectedDate = new Date();
  moduloSelectDefault = 'opcion-1';

  // Temporal (Comparativa)
  moduloDraCaja1 = [
    '203487465',
    '203487466',
    '203487467',
    '203487468',
    '203487469',
    '203487470',
    '203487471',
    '203487472',
    '203487473',
    '203487474',
    '203487475',
  ];
  moduloDraCaja2 = [
    '203487476',
    '203487477',
    '203487478',
  ];

  // Temporal (Comparativa)
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex,
      );
    }
  }

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }
  
  // Temporal (Rutas)
  routerLink(page: string, params: any) {
    if (params)
      this.router.navigate(['/'+page], { queryParams: params });
    else
      this.router.navigate(['/'+page]);
  }

}
