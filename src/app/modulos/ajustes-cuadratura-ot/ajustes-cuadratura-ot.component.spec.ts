import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjustesCuadraturaOtComponent } from './ajustes-cuadratura-ot.component';

describe('AjustesCuadraturaOtComponent', () => {
  let component: AjustesCuadraturaOtComponent;
  let fixture: ComponentFixture<AjustesCuadraturaOtComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjustesCuadraturaOtComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjustesCuadraturaOtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
