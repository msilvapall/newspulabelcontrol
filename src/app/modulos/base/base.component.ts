import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.css']
})
export class BaseComponent implements OnInit {
  moduloInfo = ['Exp Unifrutti Traders SPA', 'Temporada 2020', 'Teno'];
  moduloNav = [
    { small: 'Label Control', large: 'Label Control', url: '/' },
    { small: 'Base', large: 'Módulo Base', url: '/base' },
  ];

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  // Temporal (Rutas)
  routerLink(page: string, params: any) {
    if (params)
      this.router.navigate(['/'+page], { queryParams: params });
    else
      this.router.navigate(['/'+page]);
  }

}
