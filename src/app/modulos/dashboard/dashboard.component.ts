import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  moduloInfo = ['Exp Unifrutti Traders SPA', 'Temporada 2020', 'Teno'];
  moduloNav = [
    { small: 'Label Control', large: 'Label Control', url: '/' },
    { small: 'Dashboard', large: 'Dashboard', url: '/dashboard' },
  ];

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  // Temporal (Rutas)
  routerLink(page: string, params: any) {
    if (params)
      this.router.navigate(['/'+page], { queryParams: params });
    else
      this.router.navigate(['/'+page]);
  }

}
