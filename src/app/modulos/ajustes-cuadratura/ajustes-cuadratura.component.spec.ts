import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjustesCuadraturaComponent } from './ajustes-cuadratura.component';

describe('AjustesCuadraturaComponent', () => {
  let component: AjustesCuadraturaComponent;
  let fixture: ComponentFixture<AjustesCuadraturaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjustesCuadraturaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjustesCuadraturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
