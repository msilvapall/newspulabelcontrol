import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

// Temporal (Tabla)
export interface TableElement {
  item1: string;
  item2: string;
  item3: string;
  item4: string;
  item5: string;
}

// Temporal (Tabla)
const ELEMENT_DATA: TableElement[] = [
  {item1: 'F1', item2: 'XPF', item3: 'XJ', item4: 'Productor 1', item5: '20'},
  {item1: 'F2', item2: 'XPR', item3: 'L', item4: 'Productor 2', item5: '30'},
  {item1: 'F3', item2: 'XPF', item3: 'M', item4: 'Productor 3', item5: '40'},
  {item1: 'F4', item2: 'XPR', item3: 'S', item4: 'Productor 4', item5: '50'},
  {item1: 'F5', item2: 'XPF', item3: 'XJ', item4: 'Productor 5', item5: '60'},
];

@Component({
  selector: 'app-ajustes-cuadratura',
  templateUrl: './ajustes-cuadratura.component.html',
  styleUrls: ['./ajustes-cuadratura.component.css']
})
export class AjustesCuadraturaComponent implements OnInit {
  moduloInfo = ['Exp Unifrutti Traders SPA', 'Temporada 2020', 'Teno'];
  moduloNav = [
    { small: 'Label Control', large: 'Label Control', url: '/' },
    { small: 'Ajustes cuadratura', large: 'Listado folios por OT', url: '/ajustes-cuadratura' },
  ];

  // Temporal (Tabla)
  displayedColumns: string[] = ['item1', 'item2', 'item3', 'item4', 'item5', 'itemAcciones'];
  dataSource = ELEMENT_DATA;

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  // Temporal (Rutas)
  routerLink(page: string, params: any) {
    if (params)
      this.router.navigate(['/'+page], { queryParams: params });
    else
      this.router.navigate(['/'+page]);
  }

}
